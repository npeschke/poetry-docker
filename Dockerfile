FROM python:3.11-alpine
LABEL authors="npeschke"

ENV POETRY_VERSION=1.5.1
ENV POETRY_HOME=/opt/poetry

RUN python3 -m venv $POETRY_HOME

RUN $POETRY_HOME/bin/pip install poetry==$POETRY_VERSION
RUN $POETRY_HOME/bin/poetry --version
